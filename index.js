
const http = require('http');
const fs = require('fs');
const { RSA_NO_PADDING } = require('constants');
const { equal } = require('assert');
const server = http.createServer((req,res)=>{
    console.log("server Started !!!!");
    if(req.url == '/')
    {
        res.writeHead(200,{'Content-Type' : 'text/html'});
        res.write('<h1>This is Home page !</h1>') 
        res.end();

    }else if(req.url == '/html')
    {
        res.writeHead(200,{'Content-Type' : 'text/html'});
        fs.readFile('index.html',(error,data)=>{
            if(error)
            {
                res.writeHead(404);
                res.write("Error : File not found");
                res.end();
             
            }
            else{
                res.write(data);
            }
            res.end();
           
       })

    }else if(req.url == '/json')
    {
        res.writeHead(200,{'Content-Type' : 'application/json'});
        fs.readFile('index.json' , (error,data)=>{
            if(error)
            {
                res.writeHead(404);
                res.write("Error : File not found");
                res.end();
                
            }else{
                res.write(data);
                res.end();
            }
        })
    }else if(req.url == '/uuid')
    {
        res.writeHead(200,{'Content-Type' : 'text/html'});
        fs.readFile('uuid.txt',(error,data)=>{
            if(error)
            {
                res.writeHead(404);
                res.write("Error : File not found");
                res.end();
                
            }
            else{
                res.write(data);
            }
            res.end();
          
        })

    }else if(req.url.split('/')[1]=='status')
    {
        if(req.url.split('/')[2]=='100')
        {
            res.writeHead(200,{'Content-Type' : 'text/html'});
            res.write('<h1>Continue</h1>');
            res.end();
        }else if(req.url.split('/')[2]=='200')
        {
            res.writeHead(200,{'Content-Type' : 'text/html'});
            res.write('<h1>Ok</h1>');
            res.end();
        }else if(req.url.split('/')[2]=='300')
        {
            res.writeHead(200,{'Content-Type' : 'text/html'});
            res.write('<h1>Multiple Choise</h1>');
            res.end();

        }else if(req.url.split('/')[2]=='400')
        {
            res.writeHead(200,{'Content-Type' : 'text/html'});
            res.write('<h1>Bad Request</h1>');
            res.end();

        }else if(req.url.split('/')[2]=='500')
        {
            res.writeHead(200,{'Content-Type' : 'text/html'});
            res.write('<h1>Internal Server Error</h1>');
            res.end();
        }
    }else if(req.url.split('/')[1]=='delay')
    {
        setTimeout(()=>{
            res.writeHead(200,{'Content-Type' : 'text/html'});
            res.write(`200 Ok After ${req.url.split('/')[2]}`);
            res.end();
            return;
        },(req.url.split('/')[2])*1000);
    }else{
        res.write('<h1>This is default page</h1>');
        res.end();
    }
});

server.listen(3009,(error)=>{
    if(error)
    {
        console.log("Something went wrong");
    }else{
        console.log("You are listen Port Number 3009");
    }
})